#ifndef BUILD_ENGINE_H_
#define BUILD_ENGINE_H_

#include "cuda_runtime_api.h"
#include "logging.h"
#include "NvInfer.h"
#include "unet_utils.hpp"
#include "dcf_utils.hpp"

#define DEVICE 0
#define BATCH_SIZE 1

using namespace nvinfer1;

class engine_builder {
private:
	ICudaEngine* seg_createEngine_l(SegConfig seg_config, unsigned int maxBatchSize, IBuilder* builder, IBuilderConfig* config, DataType dt) ;

	ICudaEngine* track_createEngine_l(TrackConfig track_config, unsigned int maxBatchSize, IBuilder* builder, IBuilderConfig* config, DataType dt) ;

	void seg_APIToModel(SegConfig seg_config, Logger gLogger, unsigned int maxBatchSize, IHostMemory** modelStream) ;

	void track_APIToModel(TrackConfig track_config, Logger gLogger, unsigned int maxBatchSize, IHostMemory** modelStream) ;

public:
	void BuildEngine(SegConfig seg_config, TrackConfig track_config, Logger seg_gLogger, Logger track_gLogger) ;
};

#endif // !BUILD_ENGINE_H_

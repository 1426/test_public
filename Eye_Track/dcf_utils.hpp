#ifndef DCF_UTILS_H_
#define DCF_UTILS_H_

#include <string>
#include <cufft.h>

#define CHECK(status) \
    do\
    {\
        auto ret = (status);\
        if (ret != 0)\
        {\
            std::cerr << "Cuda failure: " << ret << std::endl;\
            abort();\
        }\
    } while (0)

class TrackConfig {
public:
	std::string model_path = "D:/tmp/Eye_Tracking/dcf.wts";
	const char* input_blob_name = "data";
	const char* output_blob_name = "prob";
	//roi size
	int szh = 512;
	int szw = 128;
	//tracking params
	float lambda0 = 1e-4;
	int track_num = 5;
	int dh = szh / track_num;
};

//this struct is for searching max value and index in array
struct infos {
	float value;
	float idx;
};

// Cuda functions for tracking
void gaussian(TrackConfig config, float sigma, float* data);

void gaussian_num(TrackConfig config, float sigma, float* data);

void gaussian_num_xy(TrackConfig config, float sigma, int* center, float* data);

void take_shaped(TrackConfig config, float sigma, float* data);

void take_shaped_num(TrackConfig config, float sigma, float* data);

void feature_mul(TrackConfig config, float* feature, float* xyc, float* result);

void fft(cufftComplex* d_data, cufftComplex* d_result, cufftHandle fftPlanFwd);

void ifft(cufftComplex* d_data, cufftComplex* d_result, cufftHandle fftPlanInv, int batch, int dataH, int dataW);

void float2cufftcomplex(TrackConfig config, float* float_data, cufftComplex* complex_data, bool with_num, bool with_channel);

void squareComplex2float(TrackConfig config, float* float_data, cufftComplex* complex_data);

void get_alphaf(TrackConfig config, cufftComplex* yf, float* kzzf, cufftComplex* alphaf);

void complex_mulconj(TrackConfig config, cufftComplex* polar_xf, cufftComplex* model_zf, cufftComplex* polar_kxzf);

void complex_mul(TrackConfig config, cufftComplex* polar_kxzf_reduced, cufftComplex* model_alphaf, cufftComplex* response);

void complex2mag(TrackConfig config, float* float_data, cufftComplex* complex_data);

void float_mul(TrackConfig config, float* response, float* filter, float* response_filtered);

void get_max_value(TrackConfig config, float* data, float* max_value, float* max_idx);

void update_complex_model(TrackConfig config, cufftComplex* model, cufftComplex* update_info, float update_k, bool with_channel);

void sum(TrackConfig config, float* data, float* result);

void complex_sum(TrackConfig config, cufftComplex* data, cufftComplex* result);

class TrackRuntime {
public:
	TrackConfig config;
	size_t size = config.track_num * config.szh * config.szw * sizeof(float);
	size_t complex_size = config.track_num * config.szh * config.szw * sizeof(cufftComplex);
	size_t zf_size = config.track_num * 64 * config.szh * config.szw * sizeof(cufftComplex);
	float* y;
	float* xy;
	float* xyc;
	float* take_shaped_labels;
	float* y_interframe;
	float* xy_interframe;
	cufftComplex* yf ;
	cufftComplex* yf_interframe;
	cufftComplex* base_model_alphaf;
	cufftComplex* base_model_zf;
	cufftComplex* model_alphaf;
	cufftComplex* model_zf;
	//cufftPlan for fft
	cufftHandle fftPlan_full;
	cufftHandle fftPlan_channel;
	cufftHandle fftPlan_num;
	int NRANK = 2;
	int NX = config.szw, NY = config.szh, num = config.track_num;
	int n[2] = { NX,NY };

	TrackRuntime() {
		cufftComplex* tmp_y;
		CHECK(cufftPlanMany(&fftPlan_full, NRANK, n, NULL, 1, NX * NY, NULL, 1, NX * NY, CUFFT_C2C, num * 64));
		CHECK(cufftPlanMany(&fftPlan_channel, NRANK, n, NULL, 1, NX * NY, NULL, 1, NX * NY, CUFFT_C2C, 64));
		CHECK(cufftPlanMany(&fftPlan_num, NRANK, n, NULL, 1, NX * NY, NULL, 1, NX * NY, CUFFT_C2C, num));
		
		CHECK(cudaMalloc((void**)&y, size));
		CHECK(cudaMalloc((void**)&xy, size));
		CHECK(cudaMalloc((void**)&xyc, size));
		CHECK(cudaMalloc((void**)&y_interframe, size));
		CHECK(cudaMalloc((void**)&xy_interframe, size));
		CHECK(cudaMalloc((void**)&take_shaped_labels, size));
		CHECK(cudaMalloc((void**)&tmp_y, complex_size));
		CHECK(cudaMalloc((void**)&yf, complex_size));
		CHECK(cudaMalloc((void**)&yf_interframe, complex_size));
		CHECK(cudaMalloc((void**)&base_model_alphaf, complex_size));
		CHECK(cudaMalloc((void**)&model_alphaf, complex_size));
		CHECK(cudaMalloc((void**)&base_model_zf, zf_size));
		CHECK(cudaMalloc((void**)&model_zf, zf_size));

		gaussian_num(config, 4.0, (float*)y);
		gaussian_num(config, 20.0, (float*)xy);
		gaussian_num(config, 20.0, (float*)xyc);
		take_shaped_num(config, 75.0, (float*)take_shaped_labels);
		float2cufftcomplex(config, y, tmp_y, 1, 0);
		fft(tmp_y, yf, fftPlan_num);
		cudaFree(tmp_y);
	}

	void update_yf_interframe(float sigma, int* center) {
		cufftComplex* tmp_y_interframe;
		CHECK(cudaMalloc((void**)&tmp_y_interframe, complex_size));
		gaussian_num_xy(config, sigma, center, y_interframe);
		float2cufftcomplex(config, y_interframe, tmp_y_interframe, 1, 0);
		fft(tmp_y_interframe, yf_interframe, fftPlan_num);
		cudaFree(tmp_y_interframe);
	}

	void update_xy_interframe(float sigma, int* center) {
		gaussian_num_xy(config, sigma, center, xy_interframe);
	}

};

#endif
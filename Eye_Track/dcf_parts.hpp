#ifndef DCF_PARTS_H_
#define DCF_PARTS_H_

#include "NvInfer.h"
#include <map>
#include <string>
#include <opencv2/opencv.hpp>

using namespace nvinfer1;

ILayer* feature(INetworkDefinition* network, std::map<std::string, Weights>& weightMap,
	ITensor& input, int outch, std::string lname, int midch) ;

void track_normalize(cv::Mat track_img, float* track_hdata, int h, int w) ;

#endif


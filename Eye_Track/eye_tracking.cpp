#include <iostream>
#include <chrono>
#include <math.h>
#include <stdio.h>
#include <opencv2/opencv.hpp>
#include "cuda_runtime_api.h"
#include <fstream>
#include <iostream>
#include "logging.h"
#include "build_engine.hpp"
#include "dcf_parts.hpp"
#include "unet_parts.hpp"
#include<ctime>

#define DEVICE 0
#define BATCH_SIZE 1
#define PI 3.1415926535
//define whether the engine has been builded
#define ENGINE_BUILDED 1

// stuff we know about the network and the input/output blobs, seg_config... has been initialized in build_engine
SegConfig seg_config;
TrackConfig track_config;

const char* INPUT_BLOB_NAME = "data";
const char* OUTPUT_BLOB_NAME = "prob";
const int INPUT_H = seg_config.input_h;
const int INPUT_W = seg_config.input_w;
const int OUTPUT_SIZE = seg_config.output_size;
const float CONF_THRESH = seg_config.conf_thresh;

const int POLAR_SIZE = track_config.szh * track_config.szw;

using namespace nvinfer1;

//this Logger is for building engine
static Logger seg_gLogger;
static Logger track_gLogger;

//this Logger is for inference
static Logger seg_run;
static Logger track_run;

void segInference(IExecutionContext& context, float* input, float* output, int batchSize) {
    const ICudaEngine& engine = context.getEngine();

    // Pointers to input and output device buffers to pass to engine.
    // Engine requires exactly IEngine::getNbBindings() number of buffers.
    assert(engine.getNbBindings() == 2);
    void* buffers[2];

    // In order to bind the buffers, we need to know the names of the input and output tensors.
    // Note that indices are guaranteed to be less than IEngine::getNbBindings()
    const int inputIndex = engine.getBindingIndex(INPUT_BLOB_NAME);
    const int outputIndex = engine.getBindingIndex(OUTPUT_BLOB_NAME);

    // Create GPU buffers on device
    CHECK(cudaMalloc(&buffers[inputIndex], batchSize * 3 * INPUT_H * INPUT_W * sizeof(float)));
    CHECK(cudaMalloc(&buffers[outputIndex], batchSize * OUTPUT_SIZE * sizeof(float)));

    // Create stream
    cudaStream_t stream;
    CHECK(cudaStreamCreate(&stream));

    // DMA input batch data to device, infer on the batch asynchronously, and DMA output back to host
    CHECK(cudaMemcpyAsync(buffers[inputIndex], input, batchSize * 3 * INPUT_H * INPUT_W * sizeof(float), cudaMemcpyHostToDevice, stream));
    context.enqueue(batchSize, buffers, stream, nullptr);
    sigmoid_conf_area((float*)buffers[outputIndex], CONF_THRESH);
    CHECK(cudaMemcpyAsync(output, buffers[outputIndex], batchSize * OUTPUT_SIZE * sizeof(float), cudaMemcpyDeviceToHost, stream));
    //流同步：通过cudaStreamSynchronize()来协调。
    cudaStreamSynchronize(stream);

    // Release stream and buffers
    cudaStreamDestroy(stream);
    CHECK(cudaFree(buffers[inputIndex]));
    CHECK(cudaFree(buffers[outputIndex]));
}

float* trackInference(IExecutionContext& context, float* input, int batchSize) {
    const ICudaEngine& engine = context.getEngine();

    size_t input_size = batchSize * 3 * track_config.szh * track_config.szw * sizeof(float);
    size_t output_size = batchSize * 64 * track_config.szh * track_config.szw * sizeof(float);
    // Pointers to input and output device buffers to pass to engine.
    // Engine requires exactly IEngine::getNbBindings() number of buffers.
    assert(engine.getNbBindings() == 2);
    void* buffers[2];

    // In order to bind the buffers, we need to know the names of the input and output tensors.
    // Note that indices are guaranteed to be less than IEngine::getNbBindings()
    const int inputIndex = engine.getBindingIndex(INPUT_BLOB_NAME);
    const int outputIndex = engine.getBindingIndex(OUTPUT_BLOB_NAME);

    // Create GPU buffers on device
    CHECK(cudaMalloc(&buffers[inputIndex], input_size));
    CHECK(cudaMalloc(&buffers[outputIndex], output_size));

    // Create stream
    cudaStream_t stream;
    CHECK(cudaStreamCreate(&stream));

    // DMA input batch data to device, infer on the batch asynchronously
    CHECK(cudaMemcpyAsync(buffers[inputIndex], input, input_size, cudaMemcpyHostToDevice, stream));
    context.enqueue(batchSize, buffers, stream, nullptr);
    //流同步：通过cudaStreamSynchronize()来协调。
    cudaStreamSynchronize(stream);

    // Release stream and buffers
    cudaStreamDestroy(stream);
    CHECK(cudaFree(buffers[inputIndex]));
    return (float*)buffers[outputIndex];
}

int main(int argc, char** argv) {
    if (!ENGINE_BUILDED) {
        engine_builder builder;
        builder.BuildEngine(seg_config, track_config, seg_gLogger, track_gLogger);
        return 0;
    }
    cudaSetDevice(DEVICE);
    // create a model using the API directly and serialize it to a stream
    char* seg_trtModelStream{ nullptr };
    char* track_trtModelStream{ nullptr };
    size_t seg_size{ 0 };
    size_t track_size{ 0 };
    std::string seg_engine_name = "unet.engine";
    std::string track_engine_name = "dcf.engine";
    //argc==2 serialize the track model
    if (argc == 3 && std::string(argv[1]) == "-d") {
        std::ifstream seg_file(seg_engine_name, std::ios::binary);
        if (seg_file.good()) {
            seg_file.seekg(0, seg_file.end);
            seg_size = seg_file.tellg();
            seg_file.seekg(0, seg_file.beg);
            seg_trtModelStream = new char[seg_size];
            assert(seg_trtModelStream);
            seg_file.read(seg_trtModelStream, seg_size);
            seg_file.close();
        }
        
        std::ifstream track_file(track_engine_name, std::ios::binary);
        if (track_file.good()) {
            track_file.seekg(0, track_file.end);
            track_size = track_file.tellg();
            track_file.seekg(0, track_file.beg);
            track_trtModelStream = new char[track_size];
            assert(track_trtModelStream);
            track_file.read(track_trtModelStream, track_size);
            track_file.close();
        }
    }
    else {
        std::cerr << "arguments not right!" << std::endl;
        return -1;
    }

    // -------------------------------------------
    cv::Mat frame;
    argv[2] = "D:/zyx_data/cataract/video/20180123150721.mpg";
    cv::VideoCapture cap(argv[2]);

    //prepare context for seg
    IRuntime* seg_runtime = createInferRuntime(seg_run);
    assert(seg_runtime != nullptr);
    ICudaEngine* seg_engine = seg_runtime->deserializeCudaEngine(seg_trtModelStream, seg_size);
    assert(seg_engine != nullptr);
    IExecutionContext* seg_context = seg_engine->createExecutionContext();
    assert(seg_context != nullptr);
    delete[] seg_trtModelStream;


    //prepare context for track
    IRuntime* track_runtime = createInferRuntime(track_run);
    assert(track_runtime != nullptr);
    ICudaEngine* track_engine = track_runtime->deserializeCudaEngine(track_trtModelStream, track_size);
    assert(track_engine != nullptr);
    IExecutionContext* track_context = track_engine->createExecutionContext();
    assert(track_context != nullptr);
    delete[] track_trtModelStream;

    TrackRuntime runtime_utils;
    int initialize = 0;
    int sum_move = 0;
    int frame_idx = 0;
    int* pre_pose = new int[track_config.track_num];
    int* ini_pose = new int[track_config.track_num];
    int pose_init = 0;

    float startTime;
    float endTime;

    while (cap.isOpened()) {
        cap.read(frame);
        frame_idx++;
        startTime = clock();//计时开始
        //segmentation part---------------------------------------------------------------------
        //resize data and normalize
        float* seg_hdata = new float[BATCH_SIZE * 3 * INPUT_H * INPUT_W];
        float* seg_odata = new float[BATCH_SIZE * INPUT_H * INPUT_W];

        cv::Mat seg_img(INPUT_H, INPUT_W, CV_8UC3);
        cv::resize(frame, seg_img, seg_img.size(), 0, 0, cv::INTER_LINEAR);
        seg_normalize(seg_img, seg_hdata, INPUT_H, INPUT_W);

        //do inference
        segInference(*seg_context, seg_hdata, seg_odata, BATCH_SIZE);

        //postprocess seg_data
        float* mask = seg_odata;
        cv::Mat mask_mat = cv::Mat(INPUT_H, INPUT_W, CV_8UC1);
        uchar* ptmp = NULL;
        for (int i = 0; i < INPUT_H; i++) {
            ptmp = mask_mat.ptr<uchar>(i);
            for (int j = 0; j < INPUT_W; j++) {
                float* pixcel = mask + i * INPUT_W + j;
                ptmp[j] = *pixcel;
            }
        }
        //don't need seg_hdata and seg_odata anymore
        delete[] seg_hdata;
        delete[] seg_odata;

        //create 'show' matrix for vis
        cv::Mat show(frame.rows / 2, frame.cols / 2, CV_8UC3);
        cv::resize(frame, show, show.size(), 0, 0, cv::INTER_LINEAR);

        //calculate center x and y. mask is 256*256
        cv::Moments M = cv::moments(mask_mat);
        float center_x = float(M.m10 / M.m00 * show.cols / INPUT_W);
        float center_y = float(M.m01 / M.m00 * show.rows / INPUT_H);

        //find the radius of img and perform logpolar
        std::vector<std::vector<cv::Point> > contours;
        std::vector<cv::Vec4i> hierarchy;
        cv::findContours(mask_mat, contours, hierarchy, cv::RETR_TREE, cv::CHAIN_APPROX_SIMPLE);
        //segmentation failed
        if (contours.size() != 1) continue;

        int maxradius = int(sqrt(show.cols / 2 * show.cols / 2 + show.rows / 2 * show.rows / 2));
        double m = show.cols / log(maxradius);
        int radius = int(log(center_y - contours[0][0].y * show.rows / INPUT_H) * m);

        cv::Mat polar(show.rows, show.cols, CV_8UC3);
        cv::logPolar(show, polar, cv::Point2f(center_x, center_y), m, cv::WARP_FILL_OUTLIERS + cv::INTER_LINEAR);

        cv::Mat cropped_polar = polar(cv::Range(0, polar.rows),
            cv::Range(std::max(0, radius - 75), std::min(radius + 75, polar.cols)));
        cv::resize(cropped_polar, cropped_polar, cv::Size(track_config.szw, track_config.szh), 0, 0, cv::INTER_LINEAR);
        //cv::imshow("cropped_polar", cropped_polar);
        //cv::waitKey(1);

        endTime = clock();
        std::cout << "The 1 run time is:" << (double)(endTime - startTime) / CLOCKS_PER_SEC << "s" << std::endl;
        //tracking part-----------------------------------------------------------------------------

        //basic stuff ---
        float* track_hdata = new float[BATCH_SIZE * 3 * POLAR_SIZE];
        float* track_feature;  //keep the output data on gpu for further inference
        track_normalize(cropped_polar, track_hdata, track_config.szh, track_config.szw);

        track_feature = trackInference(*track_context, track_hdata, BATCH_SIZE);
        //as long as we get the 'feature', don't need track_hdata anymore
        delete[] track_hdata;

        if (!initialize) {
            initialize = 1;
            //params:
            //track_feature: 'polar img' go through feature extraction 1 64 512 128
            //track_feature_gaussian: 'track_feature' go through filters of gaussian 3 64 512 128
            //track_feature_data: cufftComplex data layout of track_feature_gaussian 3 64 512 128
            //track_feature_spec: fft result of track_feature data 3 64 512 128
            //track_float_spec: float layout of track_feature_spec, also do the operation of square,sum.(torch.sum(zf**2), dim=4)
            //track_spec_reduced: reduce the channel dimension 3 1 512 128
            //alphaf 3 1 512 128

            //create fft plan for fft op

            float* track_feature_gaussian;
            CHECK(cudaMalloc((void**)&track_feature_gaussian, track_config.track_num * 64 * POLAR_SIZE * sizeof(float)));
            feature_mul(track_config, track_feature, runtime_utils.xyc, (float*)track_feature_gaussian);
            cudaFree(track_feature);//don't need track_feature

            //perform fft for track_feature_gaussian
            cufftComplex* track_feature_data;
            cufftComplex* track_feature_spec;
            CHECK(cudaMalloc((void**)&track_feature_data, track_config.track_num * 64 * POLAR_SIZE * sizeof(cufftComplex)));
            CHECK(cudaMalloc((void**)&track_feature_spec, track_config.track_num * 64 * POLAR_SIZE * sizeof(cufftComplex)));
            float2cufftcomplex(track_config, track_feature_gaussian, track_feature_data, 1, 1);
            cudaFree(track_feature_gaussian);//don't need track_feature_gaussian

            fft(track_feature_data, track_feature_spec, runtime_utils.fftPlan_full);
            CHECK(cudaMemcpy(runtime_utils.base_model_zf, track_feature_spec, track_config.track_num * 64 * POLAR_SIZE * sizeof(cufftComplex), cudaMemcpyDeviceToDevice));
            CHECK(cudaMemcpy(runtime_utils.model_zf, track_feature_spec, track_config.track_num * 64 * POLAR_SIZE * sizeof(cufftComplex), cudaMemcpyDeviceToDevice));
            cudaFree(track_feature_data);

            float* track_float_spec;
            CHECK(cudaMalloc((void**)&track_float_spec, track_config.track_num * 64 * POLAR_SIZE * sizeof(float)));
            squareComplex2float(track_config, track_float_spec, track_feature_spec);
            cudaFree(track_feature_spec);

            float* track_spec_reduced;
            CHECK(cudaMalloc((void**)&track_spec_reduced, track_config.track_num * POLAR_SIZE * sizeof(float)));
            sum(track_config, track_float_spec, track_spec_reduced);
            cudaFree(track_float_spec);

            cufftComplex* alphaf;
            CHECK(cudaMalloc((void**)&alphaf, track_config.track_num * POLAR_SIZE * sizeof(cufftComplex)));
            get_alphaf(track_config, runtime_utils.yf, track_spec_reduced, alphaf);
            cudaFree(track_spec_reduced);

            CHECK(cudaMemcpy(runtime_utils.base_model_alphaf, alphaf, track_config.track_num * POLAR_SIZE * sizeof(cufftComplex), cudaMemcpyDeviceToDevice));
            CHECK(cudaMemcpy(runtime_utils.model_alphaf, alphaf, track_config.track_num * POLAR_SIZE * sizeof(cufftComplex), cudaMemcpyDeviceToDevice));
            cudaFree(alphaf);

            continue;
        }
        //forward----------------------------------------------------------------------------------
        cufftComplex* tmp_polar_xf;
        cufftComplex* polar_xf;
        cufftComplex* polar_kxzf;
        cufftComplex* polar_kxzf_reduced;
        cufftComplex* responsef;
        cufftComplex* response;
        float* response_mag;
        float* response_filtered;

        CHECK(cudaMalloc((void**)&tmp_polar_xf, 64 * POLAR_SIZE * sizeof(cufftComplex)));
        float2cufftcomplex(track_config, track_feature, tmp_polar_xf, 0, 1);
        //should't cudaFree(track_feature), feature will be used to update

        CHECK(cudaMalloc((void**)&polar_xf, 64 * POLAR_SIZE * sizeof(cufftComplex)));
        fft(tmp_polar_xf, polar_xf, runtime_utils.fftPlan_channel);
        cudaFree(tmp_polar_xf);

        CHECK(cudaMalloc((void**)&polar_kxzf, track_config.track_num * 64 * POLAR_SIZE * sizeof(cufftComplex)));
        complex_mulconj(track_config, polar_xf, runtime_utils.model_zf, polar_kxzf);
        //shouldn't cudaFree(polar_xf), polar_xf will be used for correction

        CHECK(cudaMalloc((void**)&polar_kxzf_reduced, track_config.track_num * POLAR_SIZE * sizeof(cufftComplex)));
        complex_sum(track_config, polar_kxzf, polar_kxzf_reduced);
        cudaFree(polar_kxzf);

        CHECK(cudaMalloc((void**)&responsef, track_config.track_num * POLAR_SIZE * sizeof(cufftComplex)));
        complex_mul(track_config, polar_kxzf_reduced, runtime_utils.model_alphaf, responsef);
        cudaFree(polar_kxzf_reduced);

        CHECK(cudaMalloc((void**)&response, track_config.track_num * POLAR_SIZE * sizeof(cufftComplex)));
        ifft(responsef, response, runtime_utils.fftPlan_num, track_config.track_num, track_config.szh, track_config.szw);
        cudaFree(responsef);

        CHECK(cudaMalloc((void**)&response_mag, track_config.track_num * POLAR_SIZE * sizeof(float)));
        complex2mag(track_config, response_mag, response);
        cudaFree(response);

        CHECK(cudaMalloc((void**)&response_filtered, track_config.track_num * POLAR_SIZE * sizeof(float)));
        float_mul(track_config, response_mag, runtime_utils.take_shaped_labels, response_filtered);
        cudaFree(response_mag);

        //postprocess-------------------------------------------------------------------------------------
        //get the max_val and it's idx
        float* h_max_val = new float[track_config.track_num];
        float* h_max_idx = new float[track_config.track_num];
        float* d_max_val;
        float* d_max_idx;
        CHECK(cudaMalloc((void**)&d_max_val, track_config.track_num * sizeof(float)));
        CHECK(cudaMalloc((void**)&d_max_idx, track_config.track_num * sizeof(float)));
        get_max_value(track_config, response_filtered, d_max_val, d_max_idx);

        CHECK(cudaMemcpy(h_max_val, d_max_val, track_config.track_num * sizeof(float), cudaMemcpyDeviceToHost));
        CHECK(cudaMemcpy(h_max_idx, d_max_idx, track_config.track_num * sizeof(float), cudaMemcpyDeviceToHost));
        cudaFree(d_max_val);
        cudaFree(d_max_idx);
        //compute the movement according to adjacent frames
        int inter_move = 0;
        int nnum = 0;
        int* center = new int[track_config.track_num * 2];
        for (int i = 0; i < track_config.track_num; i++) {
            float val = h_max_val[i];
            int idx = (int)h_max_idx[i];
            int p_center_x = idx % track_config.szw, p_center_y = idx / track_config.szw;
            center[2 * i] = p_center_x; center[2 * i + 1] = p_center_y;
            if (!pose_init) {
                ini_pose[i] = p_center_y;
                pre_pose[i] = p_center_y;
            }
            if (pose_init && val > 0.8) {
                inter_move = inter_move + p_center_y - pre_pose[i];
                nnum++;
                pre_pose[i] = p_center_y;
            }
        }
        if (!pose_init) {
            pose_init = 1;
            continue;
        }
        if (nnum != 0)  inter_move = inter_move / nnum;
        sum_move += inter_move;
        //std::cout << "sum_move:" << sum_move << std::endl;
        //create the new gaussian_num_xy
        runtime_utils.update_yf_interframe(4.0, center);
        runtime_utils.update_xy_interframe(20.0, center);
        delete[] h_max_val;
        delete[] h_max_idx;
        delete[] center;

        endTime = clock();
        std::cout << "The 2 run time is:" << (double)(endTime - startTime) / CLOCKS_PER_SEC << "s" << std::endl;
        //updata_part--------------------------------------------------------------------------------------------
        float* update_z;
        CHECK(cudaMalloc((void**)&update_z, track_config.track_num * 64 * POLAR_SIZE * sizeof(float)));
        feature_mul(track_config, track_feature, runtime_utils.xy_interframe, (float*)update_z);
        cudaFree(track_feature);//don't need track_feature

        cufftComplex* update_z_data;
        cufftComplex* update_z_spec;
        CHECK(cudaMalloc((void**)&update_z_data, track_config.track_num * 64 * POLAR_SIZE * sizeof(cufftComplex)));
        CHECK(cudaMalloc((void**)&update_z_spec, track_config.track_num * 64 * POLAR_SIZE * sizeof(cufftComplex)));
        float2cufftcomplex(track_config, update_z, update_z_data, 1, 1);
        cudaFree(update_z);

        fft(update_z_data, update_z_spec, runtime_utils.fftPlan_full);
        update_complex_model(track_config, runtime_utils.model_zf, update_z_spec, 0.2, 1);
        cudaFree(update_z_data);

        float* kzzf_tmp;
        CHECK(cudaMalloc((void**)&kzzf_tmp, track_config.track_num * 64 * POLAR_SIZE * sizeof(float)));
        squareComplex2float(track_config, kzzf_tmp, update_z_spec);
        cudaFree(update_z_spec);

        float* kzzf;
        CHECK(cudaMalloc((void**)&kzzf, track_config.track_num * POLAR_SIZE * sizeof(float)));
        sum(track_config, kzzf_tmp, kzzf);
        cudaFree(kzzf_tmp);

        cufftComplex* update_alphaf;
        CHECK(cudaMalloc((void**)&update_alphaf, track_config.track_num * POLAR_SIZE * sizeof(cufftComplex)));
        get_alphaf(track_config, runtime_utils.yf_interframe, kzzf, update_alphaf);
        cudaFree(kzzf);

        update_complex_model(track_config, runtime_utils.model_alphaf, update_alphaf, 0.2, 0);
        cudaFree(update_alphaf);
        //correction part--------------------------------------------------------------------------------------
        cufftComplex* c_polar_kxzf;
        cufftComplex* c_polar_kxzf_reduced;
        cufftComplex* c_responsef;
        cufftComplex* c_response;
        float* c_response_mag;
        float* c_response_filtered;

        CHECK(cudaMalloc((void**)&c_polar_kxzf, track_config.track_num * 64 * POLAR_SIZE * sizeof(cufftComplex)));
        complex_mulconj(track_config, polar_xf, runtime_utils.base_model_zf, c_polar_kxzf);
        cudaFree(polar_xf);

        CHECK(cudaMalloc((void**)&c_polar_kxzf_reduced, track_config.track_num * POLAR_SIZE * sizeof(cufftComplex)));
        complex_sum(track_config, c_polar_kxzf, c_polar_kxzf_reduced);
        cudaFree(c_polar_kxzf);

        CHECK(cudaMalloc((void**)&c_responsef, track_config.track_num * POLAR_SIZE * sizeof(cufftComplex)));
        complex_mul(track_config, c_polar_kxzf_reduced, runtime_utils.base_model_alphaf, c_responsef);
        cudaFree(c_polar_kxzf_reduced);

        CHECK(cudaMalloc((void**)&c_response, track_config.track_num * POLAR_SIZE * sizeof(cufftComplex)));
        ifft(c_responsef, c_response, runtime_utils.fftPlan_num, track_config.track_num, track_config.szh, track_config.szw);
        cudaFree(c_responsef);

        CHECK(cudaMalloc((void**)&c_response_mag, track_config.track_num * POLAR_SIZE * sizeof(float)));
        complex2mag(track_config, c_response_mag, c_response);
        cudaFree(c_response);

        CHECK(cudaMalloc((void**)&c_response_filtered, track_config.track_num * POLAR_SIZE * sizeof(float)));
        float_mul(track_config, response_mag, runtime_utils.take_shaped_labels, c_response_filtered);
        cudaFree(c_response_mag);

        float* c_h_max_val = new float[track_config.track_num];
        float* c_h_max_idx = new float[track_config.track_num];
        float* c_d_max_val;
        float* c_d_max_idx;
        CHECK(cudaMalloc((void**)&c_d_max_val, track_config.track_num * sizeof(float)));
        CHECK(cudaMalloc((void**)&c_d_max_idx, track_config.track_num * sizeof(float)));
        get_max_value(track_config, c_response_filtered, c_d_max_val, c_d_max_idx);
        CHECK(cudaMemcpy(c_h_max_val, c_d_max_val, track_config.track_num * sizeof(float), cudaMemcpyDeviceToHost));
        CHECK(cudaMemcpy(c_h_max_idx, c_d_max_idx, track_config.track_num * sizeof(float), cudaMemcpyDeviceToHost));
        cudaFree(c_d_max_val);
        cudaFree(c_d_max_idx);
        cudaFree(response_filtered);
        cudaFree(c_response_filtered);

        int c_nnum = 0;
        int c_move = 0;
        for (int i = 0; i < track_config.track_num; i++) {
            float val = c_h_max_val[i];
            int idx = (int)c_h_max_idx[i];
            int p_center_x = idx % track_config.szw, p_center_y = idx / track_config.szw;
            if (val > 0.8) {
                c_move = c_move + p_center_y - ini_pose[i];
                c_nnum++;
            }
        }


        endTime = clock();//计时结束
        std::cout << "The all run time is:" << (double)(endTime - startTime) / CLOCKS_PER_SEC << "s" << std::endl;


        //compute the total movement
        if (c_nnum != 0) c_move = c_move / c_nnum;
        std::cout << "c_move:" << c_move << std::endl;
        //prevent the interframe tracking from failing continuously
        if (frame_idx % 20 == 0) sum_move = c_move;
        int display_move = 0.5 * sum_move + 0.5 * c_move;
        float theta = display_move * 2 * PI / track_config.szh;
        float line_h = 300.0 * tan(theta);
        cv::line(show, cv::Point(center_x - 300, center_y - line_h), cv::Point(center_x + 300, center_y + line_h), cv::Scalar(255, 0, 0), 4);
        cv::imshow("final", show);
        cv::waitKey(1);
        
    }



    // Destroy the engine
    seg_context->destroy();
    seg_engine->destroy();
    seg_runtime->destroy();


    track_context->destroy();
    track_engine->destroy();
    track_runtime->destroy();

    return 0;
}

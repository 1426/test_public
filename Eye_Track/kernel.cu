#include <math.h>
#include <iostream>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "unet_utils.hpp"
#include "dcf_utils.hpp"

#include <stdio.h>

#define CHECK(status) \
    do\
    {\
        auto ret = (status);\
        if (ret != 0)\
        {\
            std::cerr << "Cuda failure: " << ret << std::endl;\
            abort();\
        }\
    } while (0)

__global__ void sigmoid_conf_area_kernel(float* buffer, float conf) {
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	buffer[idx] = 1 / (1 + exp(-buffer[idx]));
	if (buffer[idx] > conf) {
		buffer[idx] = 255;
	}
	else buffer[idx] = 0;
}
void sigmoid_conf_area(float* buffer, float conf) {
	sigmoid_conf_area_kernel << <256, 256 >> > (buffer, conf);
}

//create the gaussian label at center. Test passed 
__global__ void gaussian_kernel(int h, int w, float sigma, float* data) {
	int idx = blockDim.x*blockIdx.x + threadIdx.x;
	int x = threadIdx.x;
	int y = blockIdx.x;
	int mesh_x = x + 1 - w / 2;
	int mesh_y = y + 1 - h / 2;
	data[idx] = mesh_x * mesh_x + mesh_y * mesh_y;
	data[idx] = exp(-data[idx] / (2 * sigma * sigma));
}
void gaussian(TrackConfig config, float sigma, float* data) {
	//data size should be equal to h*w
	int h = config.szh, w = config.szw;
	gaussian_kernel << <h, w >> > (h, w, sigma, data);
}


//cuda implementation of torch.roll. Test passed
__global__ void roll_kernel(int h, int w, int dh, int num, float* data) {
	int base_idx = blockDim.x * blockIdx.x + threadIdx.x;
	//the idx to be copyed, derived from the base matrix
	int copy_idx = (base_idx + h * w - num* dh * blockDim.x) % (h * w);
	int idx = base_idx + num * h * w;
	data[idx] = data[copy_idx];
}
void gaussian_num(TrackConfig config, float sigma, float* data) {
	//data size should be h*w*num
	int h = config.szh, w = config.szw;
	int num = config.track_num;
	int dh = h / num;
	gaussian_kernel << <h, w>> > (h, w, sigma, data);
	for (int i = 1; i < num; i++) roll_kernel << <h, w >> > (h, w, dh, i, data);
}


__global__ void roll_y_kernel(int h, int w, int dh, int num, float* template_gaussian, float* data) {
	int base_idx = blockIdx.x * blockDim.x + threadIdx.x;
	int copy_idx = (base_idx + h * w - dh * blockDim.x) % (h * w);
	int idx = base_idx + num * h * w;
	data[idx] = template_gaussian[copy_idx];
}
__global__ void roll_x_kernel(int h, int w, int dw, int num, float* rolled_y, float* data) {
	int plus = (w + threadIdx.x - dw) % w;
	int copy_idx = blockIdx.x * blockDim.x + plus;
	int idx = blockIdx.x * blockDim.x + threadIdx.x + num * h * w;
	data[idx] = rolled_y[copy_idx];
}
void gaussian_num_xy(TrackConfig config, float sigma, int* center, float* data) {
	int h = config.szh, w = config.szw;
	int num = config.track_num;
	float* template_gaussian;
	float* tmp_roll_y;
	CHECK(cudaMalloc((void**)&template_gaussian, h * w * sizeof(float)));
	CHECK(cudaMalloc((void**)&tmp_roll_y, num * h * w * sizeof(float)));
	gaussian(config, sigma, template_gaussian);
	for (int i = 0; i < num; i++) {
		int dw = center[2 * i] - w / 2;
		int dh = center[2 * i + 1] - h / 2;
		roll_y_kernel << <h, w >> > (h, w, dh, i, template_gaussian, tmp_roll_y);
		roll_x_kernel << <h, w >> > (h, w, dw, i, tmp_roll_y, data);
	}
	cudaFree(template_gaussian);
	cudaFree(tmp_roll_y);
}


__global__ void take_shaped_kernel(int h, float sigma, float* data) {
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	int bottom = h / 2 - int(sigma);
	int upper = h / 2 + int(sigma);
	if (blockIdx.x > bottom && blockIdx.x < upper) data[idx] = 1.0;
	else data[idx] = 0.0;
}
void take_shaped(TrackConfig config,  float sigma, float* data) {
	int h = config.szh, w = config.szw;
	take_shaped_kernel << <h, w >> > (h, sigma, data);
}


void take_shaped_num(TrackConfig config, float sigma, float* data) {
	int h = config.szh, w = config.szw;
	int num = config.track_num;
	int dh = h / num;
	take_shaped_kernel << <h, w >> > (h, sigma, data);
	for (int i = 1; i < num; i++) roll_kernel << <h, w >> > (h, w, dh, i, data);
}


__global__ void feature_mul_kernel(int h, int w,
	float* feature, float* xyc, float* result) {
	int channel_idx = blockIdx.y;
	int xyc_idx = blockIdx.x * blockDim.x + threadIdx.x;
	int feature_idx = channel_idx * gridDim.x * blockDim.x + blockIdx.x * blockDim.x + threadIdx.x;
	result[feature_idx] = feature[feature_idx] * xyc[xyc_idx];
}
void feature_mul(TrackConfig config, float* feature, float* xyc, float* result) {
	int num = config.track_num, h = config.szh, w = config.szw;
	int channel = 64;
	// total batch is num, param xyc+h*w is the pointer for next batch
	// feature shape 1*64*540*150 |xyc shape 3*1*540*150
	dim3 dimGrid(h, channel);
	dim3 dimBlock(w);
	for (int i = 0; i < num; i++)
		feature_mul_kernel << <dimGrid, dimBlock >> > (h, w, feature, xyc + i * h * w, result + i * channel * h * w);
}


void fft(cufftComplex* d_data, cufftComplex* d_result, cufftHandle fftPlanFwd) {
	CHECK(cufftExecC2C(fftPlanFwd, d_data, d_result, CUFFT_FORWARD));
}

__global__ void ifft_norm_kernel(cufftComplex* result, int dataH, int dataW) {
	int idx = blockIdx.y * gridDim.x * blockDim.x + blockIdx.x * blockDim.x + threadIdx.x;
	float norm = float(dataH * dataW);
	result[idx].x = result[idx].x / norm;
	result[idx].y = result[idx].y / norm;
}
void ifft(cufftComplex* d_data, cufftComplex* d_result, cufftHandle fftPlanInv, int batch, int dataH, int dataW) {
	CHECK(cufftExecC2C(fftPlanInv, d_data, d_result, CUFFT_INVERSE));
	dim3 dimGrid(dataW, batch);
	dim3 dimBlock(dataH);
	ifft_norm_kernel << <dimGrid, dimBlock >> > (d_result, dataH, dataW);
}


__global__ void float2cufftcomplex_kernel(float* float_data, cufftComplex* complex_data) {
	int idx = blockIdx.y * gridDim.x * blockDim.x + blockDim.x * blockIdx.x + threadIdx.x;
	complex_data[idx].x = float_data[idx];
	complex_data[idx].y = 0;
}
void float2cufftcomplex(TrackConfig config, float* float_data, cufftComplex* complex_data, bool with_num, bool with_channel) {
	dim3 dimGrid;
	dimGrid.x = config.szw;
	if (with_num && with_channel)	dimGrid.y = config.track_num * 64;
	else if (with_num)	dimGrid.y = config.track_num;
	else if (with_channel)	dimGrid.y = 64;
	dim3 dimBlock(config.szh);
	float2cufftcomplex_kernel << <dimGrid, dimBlock >> > (float_data, complex_data);
}


__global__ void squareComplex2float_kernel(float* float_data, cufftComplex* complex_data) {
	int idx = blockIdx.y * gridDim.x * blockDim.x + blockDim.x * blockIdx.x + threadIdx.x;
	int x = complex_data[idx].x, y = complex_data[idx].y;
	float_data[idx] = x * x + y * y;
}
void squareComplex2float(TrackConfig config, float* float_data, cufftComplex* complex_data) {
	dim3 dimGrid(config.szw, config.track_num * 64);
	dim3 dimBlock(config.szh);
	squareComplex2float_kernel << <dimGrid, dimBlock >> > (float_data, complex_data);
}


//cuda implementation for torch.sum(dim=1)
__global__ void sum_kernel(float* data, float* result) {

	//load the data to shared memory
	//threadIdx.x - channel, blockIdx.x - w, blockIdx.y - h
	extern __shared__ float sdata[];
	int tid = threadIdx.x;
	int data_idx = threadIdx.x * gridDim.x * gridDim.y + blockIdx.y * gridDim.x + blockIdx.x;
	int result_idx = blockIdx.y * gridDim.x + blockIdx.x;
	sdata[tid] = data[data_idx];
	__syncthreads();

	//do reduction within shared memory
	for (int s = blockDim.x / 2; s > 0; s >>= 1) {
		if(tid<s) sdata[tid] += sdata[tid + s];
		__syncthreads();
	}

	//write the sum of this block back to global memory
	if (tid == 0) {
		result[result_idx] = sdata[0];
	}
}
void sum(TrackConfig config, float* data, float* result) {
	//the data size is 3 64 512 128, result is 3 1 512 128
	int num = config.track_num, channel = 64, h = config.szh, w = config.szw;
	dim3 dimGrid(w, h);
	dim3 dimBlock(channel);
	for (int i = 0; i < num; i++) sum_kernel << <dimGrid, dimBlock, channel*sizeof(float) >> > (data + i * channel * h * w, result + i * h * w);
}


__global__ void get_alphaf_kernel(float lambda0, cufftComplex* yf, float* kzzf, cufftComplex* alphaf) {
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	alphaf[idx].x = yf[idx].x / (kzzf[idx] + lambda0);
	alphaf[idx].y = yf[idx].y / (kzzf[idx] + lambda0);
}
void get_alphaf(TrackConfig config, cufftComplex* yf, float* kzzf, cufftComplex* alphaf) {
	int num = config.track_num, h = config.szh, w = config.szw;
	float lambda0 = config.lambda0;
	for (int i = 0; i < num; i++) get_alphaf_kernel << <h, w >> > (lambda0, yf + i * h * w, kzzf + i * h * w, alphaf + i * h * w);
}


__global__ void complex_mulconj_kernel(cufftComplex* polar_xf, cufftComplex* model_zf, cufftComplex* polar_kxzf) {
	int idx = blockIdx.y * gridDim.x * blockDim.x + blockIdx.x * blockDim.x + threadIdx.x;
	polar_kxzf[idx].x = polar_xf[idx].x * model_zf[idx].x + polar_xf[idx].y * model_zf[idx].y;
	polar_kxzf[idx].y = -polar_xf[idx].x * model_zf[idx].y + polar_xf[idx].y * model_zf[idx].x;
}
void complex_mulconj(TrackConfig config, cufftComplex* polar_xf, cufftComplex* model_zf, cufftComplex* polar_kxzf) {
	int num = config.track_num, channel = 64, h = config.szh, w = config.szw;
	dim3 dimGrid(h, channel);
	dim3 dimBlock(w);
	for (int i = 0; i < num; i++)
		complex_mulconj_kernel << <dimGrid, dimBlock >> > (polar_xf, model_zf + i * channel * h * w, polar_kxzf + i * channel * h * w);
}


__global__ void complex_sum_kernel(cufftComplex* data, cufftComplex* result) {

	//load the data to shared memory
	//threadIdx.x - channel, blockIdx.x - w, blockIdx.y - h
	extern __shared__ cufftComplex c_sdata[];
	int tid = threadIdx.x;
	int data_idx = threadIdx.x * gridDim.x * gridDim.y + blockIdx.y * gridDim.x + blockIdx.x;
	int result_idx = blockIdx.y * gridDim.x + blockIdx.x;
	c_sdata[tid] = data[data_idx];
	__syncthreads();

	//do reduction within shared memory
	for (int s = blockDim.x / 2; s > 0; s >>= 1) {
		if (tid < s) {
			c_sdata[tid].x += c_sdata[tid + s].x;
			c_sdata[tid].y += c_sdata[tid + s].y;
		}
		__syncthreads();
	}

	//write the sum of this block back to global memory
	if (tid == 0) {
		result[result_idx] = c_sdata[0];
	}
}
void complex_sum(TrackConfig config, cufftComplex* data, cufftComplex* result) {
	//the data size is 3 64 540 150, result is 3 1 540 150
	int num = config.track_num, channel = 64, h = config.szh, w = config.szw;
	dim3 dimGrid(w, h);
	dim3 dimBlock(channel);
	for (int i = 0; i < num; i++) complex_sum_kernel << <dimGrid, dimBlock, channel * sizeof(cufftComplex) >> > (data + i * channel * h * w, result + i * h * w);
}


__global__ void complex_mul_kernel(cufftComplex* polar_kxzf_reduced, cufftComplex* model_alphaf, cufftComplex* response) {
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	response[idx].x = polar_kxzf_reduced[idx].x * model_alphaf[idx].x - polar_kxzf_reduced[idx].y * model_alphaf[idx].y;
	response[idx].y = polar_kxzf_reduced[idx].y * model_alphaf[idx].x + polar_kxzf_reduced[idx].x * model_alphaf[idx].y;
}
void complex_mul(TrackConfig config, cufftComplex* polar_kxzf_reduced, cufftComplex* model_alphaf, cufftComplex* response) {
	int num = config.track_num, h = config.szh, w = config.szw;
	for (int i = 0; i < num; i++)
		complex_mul_kernel << <h, w >> > (polar_kxzf_reduced + i * h * w, model_alphaf + i * h * w, response + i * h * w);
}


__global__ void complex2mag_kernel(float* float_data, cufftComplex* complex_data) {
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	float real = complex_data[idx].x;
	float_data[idx] = real * real;
}
void complex2mag(TrackConfig config, float* float_data, cufftComplex* complex_data) {
	int num = config.track_num, h = config.szh, w = config.szw;
	for (int i = 0; i < num; i++)
		complex2mag_kernel << <h, w >> > (float_data + i * h * w, complex_data + i * h * w);
}


__global__ void float_mul_kernel(float* response, float* filter, float* response_filtered) {
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	response_filtered[idx] = response[idx] * filter[idx];
}
void float_mul(TrackConfig config, float* response, float* filter, float* response_filtered) {
	int num = config.track_num, h = config.szh, w = config.szw;
	for (int i = 0; i < num; i++)
		float_mul_kernel << <h, w >> > (response + i * h * w, filter + i * h * w, response_filtered + i * h * w);
}


__global__ void get_max_value_kernel(float* data, float* tmp_value, float* tmp_idx, float* max_value, float* max_idx, bool first) {
	//load the data to shared memory
	extern __shared__ infos maxdata[];
	int tid = threadIdx.x;
	int data_idx = blockIdx.x * blockDim.x + threadIdx.x;
	if (first) {
		maxdata[tid].value = data[data_idx];
		maxdata[tid].idx = data_idx;
	}
	else {
		maxdata[tid].value = tmp_value[data_idx];
		maxdata[tid].idx = tmp_idx[data_idx];
	}
	__syncthreads();

	//do reduction within shared memory
	for (int s = blockDim.x / 2; s > 0; s >>= 1) {
		if (tid < s) {
			if (maxdata[tid + s].value > maxdata[tid].value) {
				maxdata[tid].value = maxdata[tid + s].value;
				maxdata[tid].idx = maxdata[tid + s].idx;
			}
		}
		__syncthreads();
	}

	//write the sum of this block back to global memory
	if (first) {
		if (tid == 0) {
			tmp_value[blockIdx.x] = maxdata[0].value;
			tmp_idx[blockIdx.x] = maxdata[0].idx;
		}
	}
	else {
		if (tid == 0) {
			*max_value = maxdata[0].value;
			*max_idx = maxdata[0].idx;
			//printf("max_value:%f\n", *max_value);
			//printf("max_idx:%f\n", *max_idx);
		}
	}
}
void get_max_value(TrackConfig config, float* data, float* max_value, float* max_idx) {
	int num = config.track_num, h = config.szh, w = config.szw;
	float* tmp_value;
	float* tmp_idx;
	CHECK(cudaMalloc((void**)&tmp_value, h * sizeof(float)));
	CHECK(cudaMalloc((void**)&tmp_idx, h * sizeof(float)));

	for (int i = 0; i < num; i++) {
		get_max_value_kernel << <h, w, w * sizeof(infos) >> > (data + i * h * w, tmp_value, tmp_idx, max_value + i, max_idx + i, 1);
		get_max_value_kernel << <1, h, h * sizeof(infos) >> > (data + i * h * w, tmp_value, tmp_idx, max_value + i, max_idx + i, 0);
	}

	cudaFree(tmp_value);
	cudaFree(tmp_idx);
}


__global__ void update_complex_model_kernel(cufftComplex* model, cufftComplex* update_info, float update_k) {
	int idx = blockIdx.y * gridDim.x * blockDim.x + blockDim.x * blockIdx.x + threadIdx.x;
	model[idx].x = (1 - update_k) * model[idx].x + update_k * update_info[idx].x;
	model[idx].y = (1 - update_k) * model[idx].y + update_k * update_info[idx].y;
}
void update_complex_model(TrackConfig config, cufftComplex* model, cufftComplex* update_info, float update_k, bool with_channel) {
	int num = config.track_num, h = config.szh, w = config.szw;
	dim3 dimGrid;
	dimGrid.x = w;
	if (with_channel)	dimGrid.y = num * 64;
	else dimGrid.y = num;
	dim3 dimBlock(h);
	if (with_channel)
		update_complex_model_kernel << <dimGrid, dimBlock >> > (model, update_info, update_k);
	else 
		update_complex_model_kernel << <dimGrid, dimBlock >> > (model, update_info, update_k);
}

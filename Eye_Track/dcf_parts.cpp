#include "dcf_parts.hpp"

ILayer* feature(INetworkDefinition* network, std::map<std::string, Weights>& weightMap,
	ITensor& input, int outch, std::string lname, int midch) {

	IConvolutionLayer* conv1 = network->addConvolutionNd(input, midch, DimsHW{ 3, 3 },
		weightMap[lname + ".0.weight"], weightMap[lname + ".0.bias"]);
	conv1->setStrideNd(DimsHW{ 1, 1 });
	conv1->setPaddingNd(DimsHW{ 1, 1 });

	IActivationLayer* relu1 = network->addActivation(*conv1->getOutput(0), ActivationType::kRELU);

	IConvolutionLayer* conv2 = network->addConvolutionNd(*relu1->getOutput(0), outch, DimsHW{ 3, 3 },
		weightMap[lname + ".2.weight"], weightMap[lname + ".2.bias"]);
	conv2->setStrideNd(DimsHW{ 1, 1 });
	conv2->setPaddingNd(DimsHW{ 1, 1 });

	IActivationLayer* relu2 = network->addActivation(*conv2->getOutput(0), ActivationType::kRELU);

	IConvolutionLayer* conv3 = network->addConvolutionNd(*relu2->getOutput(0), outch, DimsHW{ 3, 3 },
		weightMap[lname + ".4.weight"], weightMap[lname + ".4.bias"]);
	conv3->setStrideNd(DimsHW{ 1, 1 });
	conv3->setPaddingNd(DimsHW{ 1, 1 });

	ILRNLayer* lrn = network->addLRN(*conv3->getOutput(0), 5, 0.0001, 0.75, 1);

	return lrn;
}

void track_normalize(cv::Mat track_img, float* track_hdata, int h, int w) {
	int i = 0;
	for (int row = 0; row < h; ++row) {
		for (int col = 0; col < w; ++col) {
			track_hdata[i] = (float)track_img.at<cv::Vec3b>(row, col)[0] / 255.0;
			track_hdata[i + h * w] = (float)track_img.at<cv::Vec3b>(row, col)[1] / 255.0;
			track_hdata[i + 2 * h * w] = (float)track_img.at<cv::Vec3b>(row, col)[2] / 255.0;
			i++;
		}
	}
}
#include "build_engine.hpp"
#include "dcf_parts.hpp"
#include "unet_parts.hpp"
#include "common.hpp"

#define DEVICE 0
#define BATCH_SIZE 1

using namespace nvinfer1;

ICudaEngine* engine_builder::seg_createEngine_l(SegConfig seg_config, unsigned int maxBatchSize, IBuilder* builder, IBuilderConfig* config, DataType dt) {
    INetworkDefinition* network = builder->createNetworkV2(0U);
    // Create input tensor of shape {3, INPUT_H, INPUT_W} with name INPUT_BLOB_NAME
    ITensor* data = network->addInput(seg_config.input_blob_name, dt, Dims3{ 3, seg_config.input_h, seg_config.input_w });
    assert(data);

    std::map<std::string, Weights> weightMap = loadWeights(seg_config.model_path);

    // build network
    auto x1 = doubleConv(network, weightMap, *data, 32, "inc", 32);
    std::cout << "x1:" << x1->getOutput(0)->getDimensions().d[0] << ' ' << x1->getOutput(0)->getDimensions().d[1] << ' ' << x1->getOutput(0)->getDimensions().d[2] << std::endl;
    auto x2 = down(network, weightMap, *x1->getOutput(0), 64, "down1");
    std::cout << "x2:" << x2->getOutput(0)->getDimensions().d[0] << ' ' << x2->getOutput(0)->getDimensions().d[1] << ' ' << x2->getOutput(0)->getDimensions().d[2] << std::endl;
    auto x3 = down(network, weightMap, *x2->getOutput(0), 128, "down2");
    std::cout << "x3:" << x3->getOutput(0)->getDimensions().d[0] << ' ' << x3->getOutput(0)->getDimensions().d[1] << ' ' << x3->getOutput(0)->getDimensions().d[2] << std::endl;
    auto x4 = down(network, weightMap, *x3->getOutput(0), 256, "down3");
    std::cout << "x4:" << x4->getOutput(0)->getDimensions().d[0] << ' ' << x4->getOutput(0)->getDimensions().d[1] << ' ' << x4->getOutput(0)->getDimensions().d[2] << std::endl;
    auto x5 = down(network, weightMap, *x4->getOutput(0), 256, "down4");
    std::cout << "x5:" << x5->getOutput(0)->getDimensions().d[0] << ' ' << x5->getOutput(0)->getDimensions().d[1] << ' ' << x5->getOutput(0)->getDimensions().d[2] << std::endl;
    ILayer* x6 = up(network, weightMap, *x5->getOutput(0), *x4->getOutput(0), 128, 256, "up1");
    std::cout << "x6:" << x6->getOutput(0)->getDimensions().d[0] << ' ' << x6->getOutput(0)->getDimensions().d[1] << ' ' << x6->getOutput(0)->getDimensions().d[2] << std::endl;
    ILayer* x7 = up(network, weightMap, *x6->getOutput(0), *x3->getOutput(0), 64, 128, "up2");
    std::cout << "x7:" << x7->getOutput(0)->getDimensions().d[0] << ' ' << x7->getOutput(0)->getDimensions().d[1] << ' ' << x7->getOutput(0)->getDimensions().d[2] << std::endl;
    ILayer* x8 = up(network, weightMap, *x7->getOutput(0), *x2->getOutput(0), 32, 64, "up3");
    std::cout << "x8:" << x8->getOutput(0)->getDimensions().d[0] << ' ' << x8->getOutput(0)->getDimensions().d[1] << ' ' << x8->getOutput(0)->getDimensions().d[2] << std::endl;
    ILayer* x9 = up(network, weightMap, *x8->getOutput(0), *x1->getOutput(0), 32, 32, "up4");
    std::cout << "x9:" << x9->getOutput(0)->getDimensions().d[0] << ' ' << x9->getOutput(0)->getDimensions().d[1] << ' ' << x9->getOutput(0)->getDimensions().d[2] << std::endl;
    ILayer* x10 = outConv(network, weightMap, *x9->getOutput(0), 1, "outc");
    std::cout << "x10:" << x10->getOutput(0)->getDimensions().d[0] << ' ' << x10->getOutput(0)->getDimensions().d[1] << ' ' << x10->getOutput(0)->getDimensions().d[2] << std::endl;
    std::cout << "set name out" << std::endl;
    x10->getOutput(0)->setName(seg_config.output_blob_name);
    network->markOutput(*x10->getOutput(0));

    // Build engine
    builder->setMaxBatchSize(maxBatchSize);
    config->setMaxWorkspaceSize(16 * (1 << 20));  // 16MB

    std::cout << "Building Seg engine, please wait for a while..." << std::endl;
    ICudaEngine* engine = builder->buildEngineWithConfig(*network, *config);
    std::cout << "Build Seg engine successfully!" << std::endl;

    // Don't need the network any more
    network->destroy();

    // Release host memory
    for (auto& mem : weightMap)
    {
        free((void*)(mem.second.values));
    }

    return engine;
}

ICudaEngine* engine_builder::track_createEngine_l(TrackConfig track_config, unsigned int maxBatchSize, IBuilder* builder, IBuilderConfig* config, DataType dt) {
    INetworkDefinition* network = builder->createNetworkV2(0U);
    // Create input tensor of shape {3, INPUT_H, INPUT_W} with name INPUT_BLOB_NAME
    ITensor* data = network->addInput(track_config.input_blob_name, dt, Dims3{ 3, track_config.szh, track_config.szw });
    assert(data);
    std::map<std::string, Weights> weightMap = loadWeights(track_config.model_path);

    // build network
    ILayer* out = feature(network, weightMap, *data, 64, "feature.feature", 32);
    std::cout << "out:" << out->getOutput(0)->getDimensions().d[0] << ' ' << out->getOutput(0)->getDimensions().d[1] << ' ' << out->getOutput(0)->getDimensions().d[2] << std::endl;
    std::cout << "set name out" << std::endl;
    out->getOutput(0)->setName(track_config.output_blob_name);
    network->markOutput(*out->getOutput(0));

    // Build engine
    builder->setMaxBatchSize(maxBatchSize);
    config->setMaxWorkspaceSize(16 * (1 << 20));  // 16MB

    std::cout << "Building Track engine, please wait for a while..." << std::endl;
    ICudaEngine* engine = builder->buildEngineWithConfig(*network, *config);
    std::cout << "Build Track engine successfully!" << std::endl;

    // Don't need the network any more
    network->destroy();

    // Release host memory
    for (auto& mem : weightMap)
    {
        free((void*)(mem.second.values));
    }

    return engine;
}

void engine_builder::seg_APIToModel(SegConfig seg_config, Logger gLogger, unsigned int maxBatchSize, IHostMemory** modelStream) {
    // Create builder
    IBuilder* builder = createInferBuilder(gLogger);
    IBuilderConfig* config = builder->createBuilderConfig();
    // Create model to populate the network, then set the outputs and create an engine
    // ICudaEngine* engine = (CREATENET(NET))(maxBatchSize, builder, config, DataType::kFLOAT);
    ICudaEngine* engine = seg_createEngine_l(seg_config, maxBatchSize, builder, config, DataType::kFLOAT);
    assert(engine != nullptr);

    // Serialize the engine
    (*modelStream) = engine->serialize();

    // Close everything down
    engine->destroy();
    builder->destroy();
}

void engine_builder::track_APIToModel(TrackConfig track_config, Logger gLogger, unsigned int maxBatchSize, IHostMemory** modelStream) {
    // Create builder
    IBuilder* builder = createInferBuilder(gLogger);
    IBuilderConfig* config = builder->createBuilderConfig();
    // Create model to populate the network, then set the outputs and create an engine
    // ICudaEngine* engine = (CREATENET(NET))(maxBatchSize, builder, config, DataType::kFLOAT);
    ICudaEngine* engine = track_createEngine_l(track_config, maxBatchSize, builder, config, DataType::kFLOAT);
    assert(engine != nullptr);

    // Serialize the engine
    (*modelStream) = engine->serialize();

    // Close everything down
    engine->destroy();
    builder->destroy();
}

void engine_builder::BuildEngine(SegConfig seg_config, TrackConfig track_config, Logger seg_gLogger, Logger track_gLogger) {
    cudaSetDevice(DEVICE);
    // create a model using the API directly and serialize it to a stream
    char* seg_trtModelStream{ nullptr };
    char* track_trtModelStream{ nullptr };
    size_t seg_size{ 0 };
    size_t track_size{ 0 };
    std::string seg_engine_name = "unet.engine";
    std::string track_engine_name = "dcf.engine";

    IHostMemory* seg_modelStream{ nullptr };
    IHostMemory* track_modelStream{ nullptr };

    //build seg engine 
    seg_APIToModel(seg_config, seg_gLogger, BATCH_SIZE, &seg_modelStream);
    assert(seg_modelStream != nullptr);
    std::ofstream seg_p(seg_engine_name, std::ios::binary);
    if (!seg_p) {
        std::cerr << "could not open plan output file" << std::endl;
        return;
    }
    seg_p.write(reinterpret_cast<const char*>(seg_modelStream->data()), seg_modelStream->size());
    seg_modelStream->destroy();

    //build track engine
    track_APIToModel(track_config, track_gLogger, BATCH_SIZE, &track_modelStream);
    assert(track_modelStream != nullptr);
    std::ofstream track_p(track_engine_name, std::ios::binary);
    if (!track_p) {
        std::cerr << "could not open plan output file" << std::endl;
        return;
    }
    track_p.write(reinterpret_cast<const char*>(track_modelStream->data()), track_modelStream->size());
    track_modelStream->destroy();
    return;
}


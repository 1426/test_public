#ifndef UNET_COMMON_H_
#define UNET_COMMON_H_

#include <fstream>
#include <iostream>
#include <map>
#include <sstream>
#include <vector>
#include "NvInfer.h"

using namespace nvinfer1;

// TensorRT weight files have a simple space delimited format:
// [type] [size] <data x size in hex>
std::map<std::string, Weights> loadWeights(const std::string file);

int read_files_in_dir(const char* p_dir_name, std::vector<std::string>& file_names);

#endif

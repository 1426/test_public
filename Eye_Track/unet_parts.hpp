#ifndef UNET_PARTS_H_
#define UNET_PARTS_H_

#include "NvInfer.h"
#include <map>
#include <opencv2/opencv.hpp>

using namespace nvinfer1;

IScaleLayer* addBatchNorm2d(INetworkDefinition* network, std::map<std::string, Weights>& weightMap, ITensor& input, std::string lname, float eps) ;

ILayer* doubleConv(INetworkDefinition* network, std::map<std::string, Weights>& weightMap, ITensor& input, int outch, std::string lname, int midch) ;

ILayer* down(INetworkDefinition* network, std::map<std::string, Weights>& weightMap, ITensor& input, int outch, std::string lname) ;

ILayer* up(INetworkDefinition* network, std::map<std::string, Weights>& weightMap, ITensor& input1, ITensor& input2, int outch, int midch, std::string lname) ;

ILayer* outConv(INetworkDefinition* network, std::map<std::string, Weights>& weightMap, ITensor& input, int outch, std::string lname) ;

void seg_normalize(cv::Mat seg_img, float* seg_hdata, int h, int w) ;

#endif

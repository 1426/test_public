#include <string>
#include <cassert>
#include "unet_parts.hpp"

Weights empty_weight = { DataType::kFLOAT, nullptr, 0 };

IScaleLayer* addBatchNorm2d(INetworkDefinition* network, std::map<std::string, Weights>& weightMap, ITensor& input, std::string lname, float eps) {
    float* gamma = (float*)weightMap[lname + ".weight"].values;
    float* beta = (float*)weightMap[lname + ".bias"].values;
    float* mean = (float*)weightMap[lname + ".running_mean"].values;
    float* var = (float*)weightMap[lname + ".running_var"].values;
    int len = weightMap[lname + ".running_var"].count;

    float* scval = reinterpret_cast<float*>(malloc(sizeof(float) * len));
    for (int i = 0; i < len; i++) {
        scval[i] = gamma[i] / sqrt(var[i] + eps);
    }
    Weights scale{ DataType::kFLOAT, scval, len };

    float* shval = reinterpret_cast<float*>(malloc(sizeof(float) * len));
    for (int i = 0; i < len; i++) {
        shval[i] = beta[i] - mean[i] * gamma[i] / sqrt(var[i] + eps);
    }
    Weights shift{ DataType::kFLOAT, shval, len };

    float* pval = reinterpret_cast<float*>(malloc(sizeof(float) * len));
    for (int i = 0; i < len; i++) {
        pval[i] = 1.0;
    }
    Weights power{ DataType::kFLOAT, pval, len };

    weightMap[lname + ".scale"] = scale;
    weightMap[lname + ".shift"] = shift;
    weightMap[lname + ".power"] = power;
    IScaleLayer* scale_1 = network->addScale(input, ScaleMode::kCHANNEL, shift, scale, power);

    assert(scale_1);
    return scale_1;
}


ILayer* doubleConv(INetworkDefinition* network, std::map<std::string, Weights>& weightMap, ITensor& input, int outch, std::string lname, int midch) {

    //do the first convolution
    IConvolutionLayer* conv1 = network->addConvolutionNd(input, midch, DimsHW{ 3, 3 }, weightMap[lname + ".double_conv.0.weight"], empty_weight);
    conv1->setStrideNd(DimsHW{ 1, 1 });
    conv1->setPaddingNd(DimsHW{ 1, 1 });
    conv1->setNbGroups(1);
    IScaleLayer* bn1 = addBatchNorm2d(network, weightMap, *conv1->getOutput(0), lname + ".double_conv.1", 1e-05);
    IActivationLayer* relu1 = network->addActivation(*bn1->getOutput(0), ActivationType::kRELU);

    //do the second convolution
    IConvolutionLayer* conv2 = network->addConvolutionNd(*relu1->getOutput(0), outch, DimsHW{ 3, 3 }, weightMap[lname + ".double_conv.3.weight"], empty_weight);
    conv2->setStrideNd(DimsHW{ 1, 1 });
    conv2->setPaddingNd(DimsHW{ 1, 1 });
    conv2->setNbGroups(1);
    IScaleLayer* bn2 = addBatchNorm2d(network, weightMap, *conv2->getOutput(0), lname + ".double_conv.4", 1e-05);
    IActivationLayer* relu2 = network->addActivation(*bn2->getOutput(0), ActivationType::kRELU);

    assert(relu2);
    return relu2;
}


ILayer* down(INetworkDefinition* network, std::map<std::string, Weights>& weightMap, ITensor& input, int outch, std::string lname) {
    //pooling first
    IPoolingLayer* pool = network->addPoolingNd(input, PoolingType::kMAX, DimsHW{ 2, 2 });
    pool->setStrideNd(DimsHW{ 2, 2 });

    //do the double conv
    ILayer* dbconv = doubleConv(network, weightMap, *pool->getOutput(0), outch, lname + ".maxpool_conv.1", outch);

    assert(dbconv);
    return dbconv;
}


ILayer* up(INetworkDefinition* network, std::map<std::string, Weights>& weightMap, ITensor& input1, ITensor& input2, int outch, int midch, std::string lname) {

    // add upsample bilinear
    IResizeLayer* deconv1 = network->addResize(input1);
    auto input1_dimensions = input1.getDimensions();
    auto outdims = Dims3{ input1_dimensions.d[0], input1_dimensions.d[1] * 2, input1_dimensions.d[2] * 2 };
    deconv1->setOutputDimensions(outdims);
    deconv1->setResizeMode(ResizeMode::kLINEAR);
    deconv1->setCoordinateTransformation(ResizeCoordinateTransformation::kALIGN_CORNERS);

    int diffx = input2.getDimensions().d[1] - deconv1->getOutput(0)->getDimensions().d[1];
    int diffy = input2.getDimensions().d[2] - deconv1->getOutput(0)->getDimensions().d[2];

    ILayer* pad1 = network->addPaddingNd(*deconv1->getOutput(0), DimsHW{ diffx / 2, diffy / 2 }, DimsHW{ diffx - (diffx / 2), diffy - (diffy / 2) });
    ITensor* inputTensors[] = { &input2,pad1->getOutput(0) };
    auto* cat = network->addConcatenation(inputTensors, 2);

    ILayer* dbconv = doubleConv(network, weightMap, *cat->getOutput(0), outch, lname + ".conv", midch);

    assert(dbconv);
    return dbconv;
}


ILayer* outConv(INetworkDefinition* network, std::map<std::string, Weights>& weightMap, ITensor& input, int outch, std::string lname) {
    // Weights emptywts{DataType::kFLOAT, nullptr, 0};

    IConvolutionLayer* conv1 = network->addConvolutionNd(input, 1, DimsHW{ 1, 1 }, weightMap[lname + ".conv.weight"], weightMap[lname + ".conv.bias"]);
    conv1->setStrideNd(DimsHW{ 1, 1 });
    conv1->setPaddingNd(DimsHW{ 0, 0 });
    conv1->setNbGroups(1);

    assert(conv1);
    return conv1;
}

void seg_normalize(cv::Mat seg_img, float* seg_hdata, int h, int w) {
    static float mean[3] = { 0.485 * 255, 0.456 * 255, 0.406 * 255 };
    static float std[3] = { 0.229 * 255, 0.224 * 255, 0.225 * 255 };

    int i = 0;
    for (int row = 0; row < h; ++row) {
        for (int col = 0; col < w; ++col) {
            seg_hdata[i] = ((float)seg_img.at<cv::Vec3b>(row, col)[0] - mean[0]) / std[0] / 255.0;
            seg_hdata[i + h * w] = ((float)seg_img.at<cv::Vec3b>(row, col)[1] - mean[1]) / std[1] / 255.0;
            seg_hdata[i + 2 * h * w] = ((float)seg_img.at<cv::Vec3b>(row, col)[2] - mean[2]) / std[2] / 255.0;
            i++;
        }
    }
}

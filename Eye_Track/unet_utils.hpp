#ifndef UNET_UTILS_H_
#define UNET_UTILS_H_


// cuda function for accelerating image postprocessing
void sigmoid_conf_area(float* buffer, float conf);

class SegConfig {
public:
	std::string model_path = "D:/tmp/Eye_Tracking/Unet.wts";
	const char* input_blob_name = "data";
	const char* output_blob_name = "prob";

	float conf_thresh = 0.5;
	int input_h = 256;
	int input_w = 256;
	int output_size = 256 * 256;
};

#endif 
